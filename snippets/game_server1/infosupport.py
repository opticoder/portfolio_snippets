import urlparse
import json
import os
import uuid
from core import *
import utils
from gicrypt import decrypt
from logger import log, set_user_id

key = os.environ['GSRV_IKEY']
private_key_string = open(key, 'r').read()

import datetime
class FixedOffset(datetime.tzinfo):
    """Fixed offset in minutes east from UTC."""

    def __init__(self, offset, name):
        self._offset = datetime.timedelta(hours=offset)
        self._name = name

    def utcoffset(self, dt):
        return self._offset

    def tzname(self, dt):
        return self._name

    def dst(self, dt):
        return datetime.timedelta(0)

MSK = FixedOffset(4, 'MSK')
def get_date(timestamp):
    return datetime.datetime.fromtimestamp(timestamp, MSK).strftime('%d.%m.%Y %H:%M:%S')


import decimal
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        super(DecimalEncoder, self).default(o)


class ArgumentsError(Exception):
    pass


def application(environ, start_response):
    set_user_id(utils.get_client_address(environ))

    try:
        parameters = urlparse.parse_qs(environ['QUERY_STRING'])
        req = ".".join(parameters['request'])
        sig = ".".join(parameters['sig'])
    except KeyError:
        log.warning('not enough parameters')
        return make_answer(start_response, None)

    log.dump('envelope key="%s"', sig)
    log.dump('request="%s"', req)

    try:
        request_text = decrypt(private_key_string, sig, req)
    except Exception as e:
        return make_answer(start_response, None, 'request decrypting failed: %s' % e)

    log.dump('decrypted request="%s"', request_text)

    try:
        request = json.loads(request_text)
    except Exception as e:
        return make_answer(start_response, None, 'request decoding failed: %s' % e)

    try:
        action = request['action']
        params = request['params']
    except KeyError as e:
        return make_answer(start_response, None, 'invalid request: %s' % e)

    try:
        function = globals()["Action_" + action]()
    except KeyError:
        return make_answer(start_response, None, 'invalid action: %s' % action)

    try:
        result = function(params)
        # returning some correct json because we can't return just None (it treats as error)
        if result is None: result = {}
        return make_answer(start_response, result)
    except ArgumentsError as e:
        return make_answer(start_response, None, 'action parameters error: %s' % e)


def make_answer(sr, response, error=None):
    if error is None:
        if response is None:
            sr('400 Bad Request', [])
            return ""
        result = response
        result['status'] = 0
    else:
        log.error(error)
        result = {'message': error}
        result['status'] = 1

    output = json.dumps(result, cls=DecimalEncoder)
    log.dump('answer="%s"', output)
    sr('200 OK', [('Content-type', 'application/json'), ('Content-Length', str(len(output)))])
    return output


class Action(object):
    def check_integration(self, params):
        return []

    def by_support_id(self, sid, params):
        raise ArgumentsError("requesting by Support ID is unavaible")

    def by_transaction_id(self, tid, params):
        raise ArgumentsError("requesting by Transaction ID is unavaible")

    def __call__(self, params):
        try:
            sid = params['supportId']
            return self.by_support_id(sid, params)
        except KeyError:
            try:
                tid = params['transactionId']
                return self.by_transaction_id(tid, params)
            except KeyError:
                pass
            raise ArgumentsError("no support or transaction id")


class Action_checkIntegration(Action):
    def __call__(self, params):
        result = {}
        for param in params:
            try:
                command = param['command']
                arguments = param['params']
            except KeyError as e:
                raise ArgumentsError(str(e))

            try:
                action = globals()['Action_' + command]
            except KeyError:
                result_params = None
            else:
                result_params = action().check_integration(arguments)

            result_status = 1
            if result_params is None:
                result_status = 0
                result_params = []

            result[command] = {'params': result_params, 'status': result_status}

        return {'actions': result}


class Action_getInfo(Action):
    def check_integration(self, params):
        result = {}
        for param in params:
            result[param] = 0 if getattr(self, 'info_' + param, None) is None else 1
        return result

    def _get_all_info_procs(self):
        def is_info_proc(name):
            return name.startswith('info_')

        return filter(is_info_proc, dir(self))

    def by_support_id(self, sid, params):
        result = {}
        for info_proc in self._get_all_info_procs():
            result[info_proc[5:]] = getattr(self, info_proc)(sid)
        return result

    def info_profile(self, sid):
        result = {}
        try:
            user = UserInfo(sid)
        # TODO: after change db			state = StateInfo( sid )
        except UserError as e:
            raise ArgumentsError(e)

        result['supportId'] = sid
        result['registrationDate'] = get_date(user.created())
        result['gameVersion'] = user.version()
        result['deviceModel'] = user.device()
        result['deviceOs'] = user.os()
        if user.rooted():
            result['deviceOs'] += ' (root/jailbrake)'
        login = user.last_login()
        if login:
            result['lastLoginDate'] = get_date(login)
            # HACK: use data from StateInfo after change db
            result['currentLevel'] = user.parameter(StateInfo.Experience)
            result['gameCurrencyAmount'] = map(
                lambda currency: {'name': StateInfo.currencies_names[currency], 'amount': user.parameter(currency)},
                range(len(StateInfo.currencies))
            )
        result['isCheater'] = user.cheater()

        return result

    def info_transactions(self, sid):
        result = []
        try:
            user = UserID(sid)
        except UserError as e:
            raise ArgumentsError(e)
        payments = UserPaymentsInfo(user)
        # NOTE: sorted by date already

        paym = [x for x in payments.iterate(payments.PaymentID, payments.Date, payments.ProductType, payments.Price,
                                            payments.Amount, payments.StatID)]
        paym.reverse()
        for payment in paym:
            transaction = {}
            transaction['orderId'] = payment[0]
            transaction['date'] = get_date(payment[1])
            level = 0
            if payment[5] != 0:
                # FIXME: unneded?
                try:
                    state = StateInfo(sid, True, payment[5])
                    level = state.parameter(state.Experience)
                except UserError:
                    pass
            transaction['level'] = level
            transaction['realAmount'] = payment[3]
            transaction['money'] = [{'name': StateInfo.currencies_names[payment[2]], 'amount': payment[4]}]
            result.append(transaction)
        return result

    def info_hdtransactions(self, sid):
        result = []
        try:
            payments = UserChargesInfo(sid)
        except UserError as e:
            raise ArgumentsError(e)
        # NOTE: sorted by date already

        paym = [x for x in payments.iterate(payments.ChargeID, payments.Date, payments.ResourceType, payments.Amount)]
        paym.reverse()
        for payment in paym:
            transaction = {}
            transaction['id'] = payment[0]
            transaction['date'] = get_date(payment[1])
            transaction['status'] = 0  # TODO: make ability to cancel charges (server commands)
            transaction['name'] = StateInfo.currencies_names[payment[2]]
            transaction['quantity'] = payment[3]
            result.append(transaction)
        return result

    def info_accessibleItems(self, sid):
        result = []
        for resource in range(len(StateInfo.currencies_names)):
            result.append({'id': resource, 'name': StateInfo.currencies_names[resource]})
        return result


class Action_addItems(Action):
    def by_support_id(self, sid, params):
        charge = UserCharge(sid)
        for item in params['items']:
            # NOTE: tid could be provided by infosupport...
            charge.charge(int(item['id']), item['quantity'], uuid.uuid4())
