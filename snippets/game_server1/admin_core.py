import time
from logger import log
import db
import utils


def dt2ts(date):
    import calendar
    return int(calendar.timegm(date.utctimetuple()))


class UserError(Exception):
    pass


class UserID(object):
    def __init__(self, uid, sid=True):
        if sid:
            try:
                self._uid = utils.db_uid(int(uid))
            except ValueError:
                raise UserError('irregular sid format')
        else:
            self._uid = uid

        self._reg_time = db.get_register_time(self._uid)
        if not self._reg_time:
            # there's no device for the uid
            raise UserError('user not found')
        self._reg_time = dt2ts(self._reg_time)


class UserInfo(UserID):
    def __init__(self, uid, sid=True):
        super(UserInfo, self).__init__(uid, sid)
        self._device = db.read_device(db.get_active_device(self._uid))
        self._user = db.read_user_by_id(self._uid)

    def parameter(self, id):
        return self._user[StateInfo._properties_db_fields[id]]

    def last_login(self):
        return dt2ts(self._user['modified']) if self._user else None

    def created(self):
        return self._reg_time

    def device(self):
        return self._device['hardware']

    def os(self):
        return self._device['os_version']

    def version(self):
        return self._device['client_version']

    def cheater(self):
        return self._user['is_cheater'] if self._user else False

    def rooted(self):
        return self._user['is_rooted'] if self._user else False


class StateInfo(UserID):
    Candy, Gold, Experience = range(3)
    _properties_db_fields = ("runes", "gold", "exp")
    currencies = (Candy, Gold)
    currencies_names = ("Candy", "Gold")

    def __init__(self, uid, sid=True, stat_id=0):
        super(StateInfo, self).__init__(uid, sid)
        #		if not stat_id:
        self.stat = db.read_stats(self._uid, stat_id)
        if not self.stat:
            raise UserError('no state info for the user')

    def parameter(self, id):
        return self.stat[self._properties_db_fields[id]]


class _PaymentsInfo(object):
    UserID, PaymentID, ProductType, Amount, Price, Date, StatID, Index = range(8)
    _db_fields = ("user_id", "transaction_id", "product_type", "amount", "price", "purchase_date", "stat_id", "id")

    def iterate(self, *properties):
        def convert(n, field):
            if self.Date == n:
                return dt2ts(field)
            elif self.ProductType == n:
                return UserCharge._resource_ids.index(field)
            return field

        for payment in self._payments:
            yield map(lambda n: convert(n, payment[self._db_fields[n]]), properties)


class UserPaymentsInfo(_PaymentsInfo):
    def __init__(self, user):
        self._payments = db.read_payments(user._uid)


class AllPaymentsInfo(_PaymentsInfo):
    def __init__(self, from_id, max_count):
        self._payments = db.read_all_payments(from_id, max_count)


class UserChargesInfo(UserID):
    ChargeID, ResourceType, Amount, Date, Index = range(5)
    _db_fields = ("transaction_id", "resource_type", "amount", "date", "id")

    def __init__(self, uid, sid=True):
        super(UserChargesInfo, self).__init__(uid, sid)
        self._payments = db.read_charges(self._uid)

    def iterate(self, *properties):
        for payment in self._payments:
            yield map(lambda n: payment[self._db_fields[n]], properties)


class UserCharge(UserID):
    _resource_ids = ("runes", "gold")

    def __init__(self, uid, sid=True):
        super(UserCharge, self).__init__(uid, sid)

    def charge(self, resource_id, amount, transaction_id):
        self._schedule_cmd(resource_id, amount)
        db.write_charge(self._uid, transaction_id, time.time(), resource_id, amount)  # FIXME: move time into db module

    def _schedule_cmd(self, resource_id, amount):
        resource = self._resource_ids[resource_id]
        params = {}
        params["resource"] = resource
        params["amount"] = amount
        db.write_scheduled_cmd(self._uid, 'applyResource', params)
        log.info('scheduled incrementing resource "%s" by %d for uid %d', resource, amount, self._uid)


class Metrics(object):
    TIME_ONLINE = 300
    TIME_1DAY = 24 * 60 * 60
    TIME_2DAYS = 2 * TIME_1DAY
    TIME_MONTH = 30 * TIME_1DAY

    @staticmethod
    def users_registered():
        return db.get_registered_count()

    @classmethod
    def online(cls):
        return db.get_active_count(time.time() - cls.TIME_ONLINE)

    @classmethod
    def active_online(cls):
        return db.get_active_count(time.time() - cls.TIME_ONLINE, time.time() - cls.TIME_2DAYS)

    @classmethod
    def dau(cls):
        return db.get_active_count(time.time() - cls.TIME_1DAY)

    @classmethod
    def mau(cls):
        return db.get_active_count(time.time() - cls.TIME_MONTH)
