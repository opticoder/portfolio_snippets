import base64
import json
from logger import log
import utils

url_buy = 'buy.itunes.apple.com'
url_sandbox = 'sandbox.itunes.apple.com'


class ITunesVerifyCodes:
    OK = 0
    ERROR = 21002
    SANDBOX = 21007
    TEST_INTERNAL_ERROR = 123456


verify_timeout = 3

RequestError = utils.RequestError


def verify_receipt(sandbox, bid, receipt):
    # TODO: pre_verify_receipt delegate call
    receipt_base64 = '{"receipt-data":"%s"}' % base64.b64encode(receipt)
    log.dump('verifying receipt=%s', receipt_base64)
    data = utils.https_async_request_json(url_buy, '/verifyReceipt', verify_timeout, receipt_base64)

    # TODO: check bid, pid, tid before & after (TRANSACTION_MISMATCH)
    try:
        if data["status"] == ITunesVerifyCodes.SANDBOX:
            log.warning('the receipt is for sandbox')

            if (sandbox):
                data = utils.https_async_request_json(url_sandbox, '/verifyReceipt', verify_timeout, receipt_base64)
            else:
                log.warning('but sandbox is turned off!')
                return None, None

        if data["status"] != ITunesVerifyCodes.OK:
            log.warning('receipt verification failed on Apple!')
            return None, None

        log.info('receipt verification by Apple ok')
        answ_receipt = data["receipt"]

        if answ_receipt["bid"] != bid:
            log.warning('receipt verification failed: wrong bundle %s', answ_receipt["bid"])
            return None, None

        log.debug('reading receipt content')
        tid = answ_receipt["transaction_id"]
        pid = answ_receipt["product_id"]

        log.info('good receipt')
    except KeyError:
        log.error('invalid fields in receipt answer!')
        raise utils.RequestError('invalid fields in receipt answer')

    return tid, pid


def verify_mock(json_receipt_base64):
    log.info('mock receipt verification')
    resp = {}
    resp["status"] = ITunesVerifyCodes.TEST_INTERNAL_ERROR
    try:
        req = json.loads(json_receipt_base64)
    except:
        resp["exception"] = 'receipt verification request: invalid json!'
        return json.dumps(resp)
    try:
        receipt_base64 = req["receipt-data"]
    except KeyError:
        resp["exception"] = 'receipt verification request: no "receipt-data" in json!'
        return json.dumps(resp)

    receipt_json = aplist_to_json(base64.b64decode(receipt_base64))
    try:
        receipt = json.loads(receipt_json)
    except:
        resp["exception"] = 'receipt verification request: invalid receipt!'
        return json.dumps(resp)

    try:
        result = receipt["test-result"]
    except KeyError:
        resp["exception"] = 'receipt verification request: no "test-result" in receipt!'
        return json.dumps(resp)

    if result == "valid":
        resp["receipt"] = receipt
        resp["status"] = ITunesVerifyCodes.OK
    elif result == "sandbox":
        resp["receipt"] = receipt
        resp["status"] = ITunesVerifyCodes.SANDBOX
    elif result == "timeout":
        log.debug('making timeout')
        import time
        time.sleep(verify_timeout * 2)
        resp["status"] = ITunesVerifyCodes.ERROR
    elif result == "invalid_answ":
        log.debug('making invalid json answer')
        resp["status"] = ITunesVerifyCodes.OK
    # there will not the "receipt" field in the answer
    elif result == "malformed_answ":
        log.debug('making malformed json answer')
        return '{ "bad" : "json'
    else:
        resp["status"] = ITunesVerifyCodes.ERROR
    result = json.dumps(resp)
    log.info('mock receipt verification result: %s', result)
    return result


# origin: http://stackoverflow.com/a/11242838
def aplist_to_json(receipt):
    import re
    line = re.compile(r'("[^"]*")\s*=\s*("[^"]*");')
    trailingcomma = re.compile(r',(\s*})')
    return trailingcomma.sub(r'\1', line.sub(r'\1: \2,', receipt))
