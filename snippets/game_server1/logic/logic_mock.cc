#include "wrapper.h"
#include <cassert>
#include <cstring>
#include <string>
#include <iostream>
#include <sstream>
#include <map>
#include <list>

using namespace std;

struct PackIter
{
    PackIter( const char* str ) : data( str ) { }
    bool next() const { return ( !getline( data, result ) == false ); }
    string& get() const { return result; }
    mutable istringstream data;
    mutable string result;
};

log_line_callback put_log = 0;
#define LOG(msg) { ostringstream line; line << "mock: " << msg; put_log( line.str().c_str() ); }

const unsigned MAX_PARTS = 2;
const unsigned ID_STATE = 0;

struct game_data
{
//	const char* set_state( string s ) { state = "{ \"state\" : " + s + " }"; LOG("setting state=<" << state << ">"); return state.c_str(); }
	unsigned last_id;
	unsigned current_part;
	map< unsigned, string > state[MAX_PARTS];
	const char* set_item( unsigned part, unsigned id, string val, bool add=false ) { state[ part ][ id ] = val; if (add) callbacks.entity_added( part, id, val.c_str() ); else callbacks.entity_changed( part, id, val.c_str() ); }
	const char* del_item( unsigned part, unsigned id ) { state[ part ].erase( id ); callbacks.entity_removed( part, id ); }
	const char* new_item( string val ) { set_item( current_part, ++last_id, val ); }
	string json;
	string resource_val;

	game_callbacks callbacks;

	map<string, pair<string, unsigned> > parameters_inits;
	void init_parameter( const char* parameter, const char* value );
	int resource_offset( string parameter_name );
};

void game_data::init_parameter( const char* parameter, const char* value )
{
	parameters_inits[ parameter ] = pair<string, unsigned>( value, parameters_inits.size() );
}

int game_data::resource_offset( string parameter_name )
{
	map<string, pair<string, unsigned> >::const_iterator index = parameters_inits.find( parameter_name );
	if ( parameters_inits.end() == index )
	{
		LOG("parameter have not found");
		return -1;
	}
	unsigned offset = index->second.second;
	LOG("parameter offset: " << offset);

	return offset;
}

void* game_create( struct game_callbacks callbacks )
{
	put_log = callbacks.log_line;
	LOG("mock game creating");

	game_data* data = new game_data;
	data->callbacks = callbacks;

	data->init_parameter( kRuneResourceRequest,"0" );
	data->init_parameter( kGoldResourceRequest, "0" );
	data->init_parameter( kPlayerExpRequest, "1" );
	data->init_parameter( kPlayerCultureRequest, "2" );
	data->init_parameter( kCompleteQuestRequest, "q0" );
	data->init_parameter( kTerritoryCountRequest, "4" );

	return data;
}

static string cmd_result;
const char* game_get_cmd_add_resource( const char* id, const char* resource_name, unsigned value )
{
	ostringstream s;
	s << "{\"cmd\" : \"0a" << resource_name << value << '|' << id << "\"}";// << ends;
	cmd_result = s.str();
	LOG("generated command:<" << cmd_result << ">");
	return cmd_result.c_str();
}

const char* kGoldResourceRequest = "gold";
const char* kRuneResourceRequest = "candy";
const char* kPlayerExpRequest = "experience";
const char* kPlayerCultureRequest = "culture";
const char* kCompleteQuestRequest = "quests";
const char* kTerritoryCountRequest = "territories";

const char* kResourceTypeGold = "g";
const char* kResourceTypeRunes = "c";

const unsigned PARAMETERS_OFFSET = 100;

const char* game_get_info( void* instance, const char* parameter_name )
{
	LOG("get save parameter: " << parameter_name);

	game_data* data = reinterpret_cast<game_data*>( instance );

	int offset = data->resource_offset( parameter_name );
	if ( -1 == offset )
		return "-";
	offset += PARAMETERS_OFFSET;

	data->resource_val = data->state[ 0 ][ offset ];
	LOG("parameter value: " << data->resource_val);
	return data->resource_val.c_str();
}

int game_load_empty( void* instance )
{
	game_data* data = reinterpret_cast<game_data*>( instance );
	data->current_part = 1;
	data->last_id = 0;
	return true;
}

int game_load_initial( void* instance )
{
	game_data* data = reinterpret_cast<game_data*>( instance );
	data->current_part = 1;
	data->callbacks.part_changed( data->current_part );

	data->callbacks.entity_added( 0, ID_STATE, "initial" );

	for ( map<string, pair<string, unsigned> >::const_iterator index = data->parameters_inits.begin(); index != data->parameters_inits.end(); ++index )
		data->callbacks.entity_added( 0, PARAMETERS_OFFSET + index->second.second, index->second.first.c_str() );

	return true;
}

int game_load_entity( void* instance, unsigned part, unsigned id, const char* value )
{
	LOG("load_entity part=" << part << " id=" << id << " value=" << value);
	game_data* data = reinterpret_cast<game_data*>( instance );
	data->state[ part ][ id ] = value;
	if ( id > data->last_id ) data->last_id = id;
	return true;
}

int game_load_entity_done( void* instance )
{
	return true;
}

enum exec_result game_execute( void* instance, const void* cmd )
{
	game_data* data = reinterpret_cast<game_data*>( instance );
    string& input = reinterpret_cast<const PackIter*>( cmd )->get();
	LOG("executing cmd=" << input);
    assert( input.length() >= 2 );
	exec_result status = static_cast<exec_result>(input[0] - '0');//1st cmd byte is status (exec_result + '0')
	LOG("going to return status=" << status);

/*
 * 0 (s) - state
 * 1 (c) - command
 * n - new entity
 * e - change (edit) entity
 * d - remove (delete) entity
 * g - read (get) entity (copy value to state)
 * i - invalid state
 * r - receipt
 * a - add resources
 * p - set parameter: q - quests, e - experiense
 */
    if (input[1] == '1')     //2nd cmd bype is flag wether return a cmd content ('1') or state ('0')
    {
	LOG("returning cmd data");
//		return data->set_state( string("\"") + (input.c_str() + 2) + string("\"") );
		data->set_item( 0, ID_STATE, input.c_str() + 2 );
//		return status;
    }
	else if (input[1] == 'n')
	{
		istringstream str( input.c_str() + 2 );
		unsigned id;
		string val;
		str >> id >> val;
		data->set_item( data->current_part, id, val, true );
	}
	else if (input[1] == 'd')
	{
		istringstream str( input.c_str() + 2 );
		unsigned id;
		str >> id;
		data->del_item( data->current_part, id );
	}
	else if (input[1] == 'g')
	{
		istringstream str( input.c_str() + 2 );
		unsigned id;
		str >> id;
		map< unsigned, string >::const_iterator it = data->state[ data->current_part ].find( id );
		data->set_item( 0, ID_STATE, ( it != data->state[ data->current_part ].end() ) ? (*it).second : "none" );
	}
	else if (input[1] == 'r')
	{
		LOG("mock calling verify receipt");
		if ( data->callbacks.verify_receipt( input.c_str() + 2 ) )
		{
			LOG("good receipt");
			status = execOK;
//			return data->set_item( "{ \"state\" : " + data->state + ", \"receipt\" : \"good\"}" );
			data->set_item( 0, ID_STATE, "good" );
//			return status;
		}
		else
		{
			LOG("bad receipt");
			status = execFAIL;
			data->set_item( 0, ID_STATE, "good" );
//			return data->set_state( "\"bad receipt\"" );
			data->new_item( "bad" );
		}
	}
	else if (input[1] == 'a')
	{
		LOG("mock add resource cmd");
		size_t delim = input.find( '|', 2 );
		assert( string::npos != delim );
		if ( data->callbacks.verify_scheduled_cmd( input.c_str() + delim + 1 ) )
		{
			string s = string( 1, input[2] );
			string resource;
			if ( kResourceTypeGold == s )
				resource = kGoldResourceRequest;
			else if ( kResourceTypeRunes == s )
				resource = kRuneResourceRequest;
			//TODO: else error
			int offset = data->resource_offset( resource );
			assert( offset != -1 );
//			data->set_item( 0, ID_STATE, "{ \"state\" : " + data->state[ 0 ][ ID_STATE ] + ", \"resource" + input.c_str()[2] + "\" : " + input.substr( 3, delim-3 ) + "}" );
			data->set_item( 0, PARAMETERS_OFFSET + offset, input.substr( 3, delim-3 ) );
		}
	}
	else if (input[1] == 'p')
	{
		LOG("mock set parateter cmd");
		string p = string( 1, input[2] );
		string resource;
		if ( "e" == p )
			resource = kPlayerExpRequest;
		else if ( "q" == p )
			resource = kCompleteQuestRequest;
		//TODO: else error
		int offset = data->resource_offset( resource );
		assert( offset != -1 );
		data->set_item( 0, PARAMETERS_OFFSET + offset, input.substr( 3, string::npos ) );
	}
    else
    {
	LOG("returning state data");
//	return data->state.c_str();
    }
	LOG("returning status");
	return status;
}

const char* game_get_raw( void* instance )
{
	game_data* data = reinterpret_cast<game_data*>( instance );
	string state="empty";
	for ( size_t part = 0; part < MAX_PARTS; part++ )
		for ( map< unsigned, string >::const_iterator it = data->state[ part ].begin(); it != data->state[ part ].end(); it++ )
		{
			if ( part == 0 && it->first == ID_STATE )
				state = it->second;
		}
	data->json="{\"state\":\"" + state + "\"}";
	return data->json.c_str();
}

void game_unload( void* instance )
{
	game_data* data = reinterpret_cast<game_data*>( instance );
	for ( size_t part = 0; part < MAX_PARTS; part++ )
		data->state[ part ].clear();
}

void game_destroy( void* instance )
{
	LOG("mock game destroying");
	game_data* data = reinterpret_cast<game_data*>( instance );
	delete data;
	LOG("destroying done");
}

const void* pack_iter_begin( const char* pack ) { PackIter* iter = new PackIter( pack ); return iter->next() ? iter : 0; }
const char* pack_iter_value( const void* cmd ) { return reinterpret_cast<const PackIter*>( cmd )->get().c_str(); }
const void* pack_iter_next( const void* cmd ) { return reinterpret_cast<const PackIter*>( cmd )->next() ? cmd : 0; }
