# -*- coding: utf-8 -*-

import os
import hashlib
import urlparse
from core import AllPaymentsInfo, Metrics
from logger import log, set_user_id
from utils import get_client_address, support_id

MONEY_NAME = 'USD'
key = os.environ['GSRV_STATKEY']


class Error:
    Processing = 1
    Auth = 5
    UnkRequest = 7


def application(environ, start_response):
    set_user_id(get_client_address(environ))

    try:
        # NOTE: it's needed to replace ';' with ',' just because urlparse module is not ready that some dumps from GI can break RFC specs
        # http://www.w3.org/TR/1999/REC-html401-19991224/appendix/notes.html#h-B.2.2
        lparameters = urlparse.parse_qsl(environ['QUERY_STRING'].replace(';', ','))
        parameters = dict(lparameters)

        target = parameters['target']
        sig = parameters['sig']
    except KeyError:
        log.warning('not enough parameters')
        return error_answer(start_response, None)

    try:
        if target == "metrics":
            params = parameters['metrics']
            print params
            func = get_metrics
            log.info('request for metrics: %s', params)
        elif target == "payments":
            try:
                params = int(parameters['tid'])
            except ValueError:
                error_answer(start_response, None)
            func = get_payments
            log.info('request for payments from tid: %s', params)
        else:
            log.warning('invalid target')
            return error_answer(start_response, Error.UnkRequest)
    except KeyError:
        log.warning('not enough parameters')
        return error_answer(start_response, None)

    del lparameters[[x[0] for x in lparameters].index("sig")]

    lparameters.sort(key=lambda param: param[0])

    valid_sig = hashlib.md5()
    for param in lparameters:
        valid_sig.update("%s=%s" % (param[0], param[1].replace(',', ';')))  # see pred NOTE
    valid_sig.update(key)
    digest = valid_sig.hexdigest()

    if sig != digest:
        log.warning('not valid sig, valid: "%s"', digest)
        return error_answer(start_response, Error.Auth)

    output = func(params)
    start_response('200 OK', [('Content-type', 'application/xml'), ('Content-Length', str(len(output)))])
    log.dump('answer: %s', output)
    return output


xml_header = '<?xml version="1.0" encoding="UTF-8" ?>\n'


def wrap4XML(list_name, item_name, params_name, params_list):
    params_format = " ".join([name + '="%s"' for name in params_name])
    result = ""
    for params in params_list:
        result += '<' + item_name + ' ' + params_format % params + ' />\n'
    return xml_header + '<' + list_name + '>\n' + result + '</' + list_name + '>'


gi_metrics_map = {
    "1": lambda: Metrics.online(),
    "2": lambda: Metrics.active_online(),
    "3": lambda: Metrics.users_registered(),
    "4": lambda: Metrics.dau(),
    "5": lambda: Metrics.mau(),
}


def get_metrics(metrics_types):
    result = []
    # using set to reduce possible duplicate metrics types
    for metric in set(metrics_types.split(',')):  # see pred NOTE
        try:
            func = gi_metrics_map[metric]
            result.append((metric, func()))
        except KeyError:
            log.warning('invalid metric type: %s', metric)
    return wrap4XML('statistics', 'metrics', ("id", "value"), result)


def get_payments(last_tid):
    # TODO: move const param print (source) to wrap4XML
    payments = AllPaymentsInfo(last_tid, 10000)
    # convert float payment[7] (timestamp) to int because GI's "UNIX_TIMESTAMP" is seems like (WTF?) integer (MySQL term)
    result = map(
        lambda payment: (payment[0], support_id(payment[1]), payment[2], int(payment[3]), MONEY_NAME),
        payments.iterate(payments.Index, payments.UserID, payments.Amount, payments.Date)
    )
    log.info('total payments: %d', len(result))
    return wrap4XML('billing', 'payment', ("tid", "uid", "amount", "dt", "source"), result)


def error_answer(sr, code, text=""):
    if code == None:
        sr('400 Bad Request', [])
        return ""
    output = xml_header + '<error code="%d">%s</error>' % (code, text)
    sr('200 OK', [('Content-type', 'application/xml'), ('Content-Length', str(len(output)))])
    return output
