debug()
{
echo $* 1>&2
}

compare()
{
debug -n "compare '$1' '$2' : "
[ x"$1" = x"$2" ] || { debug failed; finish 1; }
debug ok
}

compare_files()
{
debug -n "compare files '$1' '$2' : "
diff -u $1 $2 1>&2 || { debug failed; finish 1; }
debug ok
}

finish()
{
  exit 1
}

suite_path="$1"
testname="$2"
suites_root=`dirname "$suite_path"`
home="$4"
rand="$3"
shift 4
. $suites_root/pcommon.sh
. $suite_path/common
. "$suite_path/$testname"

# HACK
suite=`basename $suite_path`
log="$suite.$testname.log"

init "$@"

#echo Configure starting... 1>&2> $log
configure || { echo Configure failed 1>&2; exit 1; }
echo "+"
echo Configure done 1>&2

run || exit 1
echo "+"
