framework_runtest()
{
  local path="$1"
  local test="$2"
  local base=`dirname "$path"`
  shift 1
  rdmd -L-l:libcurl.so.4 -J"$base" -J"$path" -I"$home/frameworks/$fw" "$path/$test" "$@"
}
