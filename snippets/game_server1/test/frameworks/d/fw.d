import std.stdio;
import std.process;
import std.string;
import std.exception;
import std.parallelism;
import std.typecons;
import std.random;
import std.conv;
import std.path;
import std.net.curl;
import etc.c.curl : CurlOption;
import std.uri;
import std.math;
import std.datetime;


Mt19937 gen;

uint rand()
{
//	Mt19937 gen;
//	gen.seed();
	return gen.front;
}



auto process_start(alias func, Args...)(Args args)
{
	auto theTask = task!func(args);
	theTask.executeInNewThread();
	return theTask;
}

auto process_wait(alias proc, Args...)()
{
	return proc.yieldForce();
}

/*
void assert_equal( alias reference, alias value, alias msg, Args... )()
{
	log( "comparing \"" ~ reference ~ "\"" );
	log( "and \"" ~ value ~ "\"" );
	if ( reference != value )
	{
		log( "not equal" );
		enforce( false, msg );
	}
	else
		log( "equal" );
}
*/
void assert_eq( T, int neq = false )( T reference, T value, string msg = "" )
{
	log( "checking values \"" ~ to!string( reference ) ~ "\"" );
	log( "  and \"" ~ to!string( value ) ~ "\" for " ~ (neq ? "inequality" : "equality") ~":" );
	if ( (reference != value) ^ neq )
	{
		log( "  not equal" );
		enforce( false, ( msg.length ? msg : "check failed" ) );
	}
	else
		log( "  equal" );
}

void assert_neq( T )( T reference, T value, string msg = "" )
{
	assert_eq( T, true )( reference, value, msg );
}

void assert_delta( T )( T reference, T value, T delta, string msg = "" )
{
	log("checking values \"" ~ to!string( reference ) ~ "\"");
	log("  and \"" ~ to!string( value ) ~ "\" for distance "~to!string(delta)~":");
	if ( abs( reference - value ) <= delta ) return;
	enforce( false, ( msg.length ? msg : "check failed" ) );
}

auto utctime()
{
	return Clock.currTime( UTC() ).toUnixTime();
}

import std.xml;

auto parse_xml( string doc, string starting_tag, string[] attributes )
{
	auto xml = new DocumentParser( doc );
	string[][] result;
	xml.onStartTag[ starting_tag ] = ( ElementParser xml )
	{
		string[] line_attrs;
		foreach( attr; attributes )
			line_attrs ~= xml.tag.attr[ attr ];
		result ~= line_attrs;
	};
	xml.parse();
	return result;
}

/*
void check( bool valid, string msg )
{
	enforce( valid, msg );
}
*/
void log( T )( T v )
{
	stderr.writeln( v );
}

auto execute( string prog, string[] params = [], string input = null, string[ string ] env = null )
{
  string[] cmd = [ prog ];
  cmd ~= params;
  Redirect r = Redirect.stdout | Redirect.stderr;
  if ( input !is null )
    r |= Redirect.stdin;

  log("execute: " ~ cmd);

  auto pipes = pipeProcess( cmd, r, env );
  scope(exit) wait( pipes.pid );

  if ( input !is null )
  {
    pipes.stdin().write( input );
    pipes.stdin.close;
  }

  // Store lines of output.
  string[] output;
  foreach (line; pipes.stdout.byLine) output ~= line.idup;

  // Store lines of errors.
  string[] errors;
  foreach (line; pipes.stderr.byLine) errors ~= line.idup;
  log("errors: " ~ errors);
  return tuple( output, errors );
}

auto http_post( string url, string data, string[string] headers )
{
  auto client = HTTP( url );
  client.handle.set(CurlOption.ssl_verifypeer, 0);
  client.handle.set(CurlOption.ssl_verifyhost, 0);
//  log( "cert=" ~ home ~ "/../developer.crt" );
//  client.caInfo( home ~ "/../developer.crt" );

  if ( data !is null )
    client.postData = data;
  foreach( header; headers.byKey() )
    client.addRequestHeader( header, headers[ header ] );
  ubyte[] response_data;
  client.onReceive = ( ubyte[] data ) { response_data ~= data; return data.length; };
  string[string] response_headers;
  client.onReceiveHeader = ( in char[] key, in char[] value ) { response_headers[ key ] = cast( string )value; };
  client.perform();
  string status = to!string( client.statusLine().code ) ~ " " ~ client.statusLine().reason;
  return tuple( status, cast( string )response_data, response_headers );
}

auto http_get( string url, string[string] headers )
{
  return http_post( url, null, headers );
}

string url_encode( string url )
{
//  return encode( url );
  return encodeComponent( url );
}

string read_env( string name, string def = null )
{
  return environment.get( name, def );
}

extern(C)
{
	void configure();
	void test();
	void clean();
}

string home;
string testname;

void init();

//import common;

//	void configure();
//	void test();
//	void clean();

//mixin(import("testwrapper.d"));

int main( string[] args )
{
  if ( args.length < 4 )
    enforce( false, "not enough arguments for the test" );

  testname = args[ 1 ];
  gen.seed( to!int( args[ 2 ] ) );
  home = args[ 3 ];

  init( args[ 4 .. $ ] );
//  did = to!string( rand() );
try
{
//	import test1;
//	writeln("configure");
//	debug( "Configure..." )
	configure();
	writeln("+");
	log( "Configure done" );

//	debug( "Running test..." )
	test();
//	debug( "Test done" )
	writeln("+");
	log( "Test passed" );
//	debug( "Tear down test..." )
	clean();
//	debug( "Done" )
	return 0;
}
catch(Exception e)
{
	writeln("-");
	log("Test failed: " ~ e.msg);
	return 1;
}

}

mixin(import("pcommon.d"));
