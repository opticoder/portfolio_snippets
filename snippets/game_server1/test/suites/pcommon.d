string host;
string hosts;
string srv_ver;
string did;

void init( string[] args )
{
  host = args[ 0 ];
  hosts = args[ 1 ];
  srv_ver = args[ 2 ];
  did = args[ 3 ];
}

//string http_get( const string url, const string[ string ] params )
auto gsrv_get( string url, string[ string ] params, string post = "-" )
{
  string[ string ] headers;
  foreach ( param; params.byKey )
  {
    string value = params[ param ];
    switch( param )
    {
    case "did": headers[ "X-Device-ID" ] = value; break;
    case "sid": headers[ "X-Session-ID" ] = value; break;
    default: assert_eq(0,1,"invalid param");
    }
  }

  string addr = "https://"~ hosts ~ url;
  log( "addr=" ~ addr );
  auto res = http_post( addr, (post == "-") ? null : post, headers);

  return tuple( res[ 0 ], res[ 1 ], res[ 2 ] );
}

auto gsrv_post( string url, string[ string ] params, string post )
{
  return gsrv_get( url, params, post );
}
