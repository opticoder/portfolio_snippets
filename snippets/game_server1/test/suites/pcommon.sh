init()
{
host="$1"
hosts="$2"
game_version="$3"
did="$4"
reg_path="/$game_version/reg/"
save_path="/$game_version/save/"
hunt_path="/$game_version/hunt/"
}

get_state()
{
field="state"
[ -n "$2" ] && field="$2"
echo "$1" | python -c "import json,sys; obj=json.load(sys.stdin); print obj[\"$field\"]"
}

get_state_json()
{
echo "$@" | python -c 'import json,sys; obj=json.load(sys.stdin); print json.dumps(obj["state"])'
}
