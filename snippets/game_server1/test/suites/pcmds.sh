test_setargs()
{
  PATH="$PATH:$home/tools"
  host=${1:-localhost}
  hosts=${host}:${3:-8088}
  host=${host}:${2:-8089}
  if [ -z "$DID" ]; then
    local _DID="$test-test-device-$RANDOM"
  else
    local _DID="$DID"
  fi
  if [ -z "$VERSION" ]; then
    VERSION=$(cat $home/../../prehistorix/version)
    eval VERSION=$VERSION
  fi
  test_args="$host $hosts $VERSION $_DID"
}
