mixin(import("common.dinc"));extern(C):

long reg_date;
string sid;

void configure()
{
  auto res = reg_device( did );
  reg_date = utctime();
  sid = read_json( res[ 1 ], "user" );
  log("sid="~sid);
}

void test()
{
  string result = request_infosupport( "getInfo" ,"{\"supportId\":\""~sid~"\"}" );
  auto msg = parse_infosupport( result, "profile" );

  log( msg );

  string status = msg[ 0 ];
  assert_eq( "0", status );

  void test_val( string id, string val )
  {
    assert_eq( val, read_json( msg[ 1 ], id ) );
  }
  void test_val_int( string id, long val )
  {
    assert_eq( val, read_json_int( msg[ 1 ], id ) );
  }
  void test_val_bool( string id, bool val )
  {
    assert_eq( val, read_json_bool( msg[ 1 ], id ) );
  }
  void test_val_time( string id, ulong val )
  {
    assert_delta( val, str2ts( read_json( msg[ 1 ], id ), "%d.%m.%Y %H:%M:%S", +4 ), 2UL );
  }

  test_val( "gameVersion", "1.5.0" );
  test_val_int( "currentLevel", 0 );
  test_val( "deviceOs", "test" );
  test_val( "deviceModel", "ipad" );
  test_val( "supportId", sid );
  test_val_bool( "isCheater", false );
  test_val_time( "registrationDate", reg_date );
  test_val_time( "lastLoginDate", reg_date );
  void test_val_list( S )( S[] vals )
  {
  
  
  }
/*
  "gameCurrencyAmount": [
    {
      "amount": 0,
      "name": "Candy"
    },
    {
      "amount": 0,
      "name": "Gold"
    }
  ],
*/
//  test_val_money( "gameCurrencyAmount", tuple( "amount", "name" ) );

  msg = parse_infosupport( result, "accessibleItems" );
  log( msg );
}

void clean()
{
}
