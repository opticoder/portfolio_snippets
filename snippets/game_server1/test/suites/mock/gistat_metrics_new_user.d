mixin(import("common.dinc"));extern(C):

struct MetricsStat
{
    uint online;
    uint active_online;
    uint users_registered;
    uint dau;
    uint mau;
}

/* metrics output example:
<?xml version="1.0" encoding="UTF-8" ?>
<statistics>
<metrics id="1" value="5" />
<metrics id="3" value="409" />
<metrics id="2" value="0" />
<metrics id="5" value="409" />
<metrics id="4" value="265" />
</statistics>
*/

auto metrics_info( string metrics )
{
  int[ int ] stat;
  foreach( attrs; parse_xml( metrics, "metrics", [ "id", "value" ] ) )
    stat[ to!int(attrs[ 0 ]) ] = to!int(attrs[ 1 ]);
  MetricsStat m;
  foreach (n, ref member; m.tupleof)
    member = stat[ n + 1 ];
  return m;
}

string sid;
MetricsStat old_metrics;

void configure()
{
  auto metrics = request_metrics( "1;2;3;4;5" );
  old_metrics = metrics_info( metrics[ 1 ] );
  log( old_metrics );

  auto res = reg_device( did );
  sid = read_json( res[ 1 ], "user" );
}

void test()
{
  auto metrics = request_metrics( "1;2;3;4;5" );
  auto new_metrics = metrics_info( metrics[ 1 ] );
  log( new_metrics );

  assert_eq( old_metrics.online + 1, new_metrics.online );
  assert_eq( old_metrics.active_online, new_metrics.active_online );
  assert_eq( old_metrics.users_registered + 1, new_metrics.users_registered );
  assert_eq( old_metrics.dau + 1, new_metrics.dau );
  assert_eq( old_metrics.mau + 1, new_metrics.mau );
}

void clean()
{
}
