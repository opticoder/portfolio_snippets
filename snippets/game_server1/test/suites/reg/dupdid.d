mixin(import("common.dinc"));extern(C):

void configure()
{
  auto res = reg_device( did, did ~ "-1" );
  assert_eq( "200 OK", res[0], "invalid code: " ~ res[0] );
}

void test()
{
  auto res = reg_device( did, did ~ "-2" );
  assert_eq( "403 Forbidden", res[0], "invalid code: " ~ res[0] );
}

void clean()
{

}
