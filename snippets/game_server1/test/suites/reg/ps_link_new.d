import std.stdio;
mixin(import("common.dinc"));extern(C):

string sid;

void configure()
{
  auto res = reg_device( did ~ "1", did, "ps" ~ did );
  sid = res[1];
}

void test()
{
  auto res = reg_device( did ~ "2", did, "ps" ~ did );
  assert_eq( sid, res[1] );
}

void clean()
{
}
