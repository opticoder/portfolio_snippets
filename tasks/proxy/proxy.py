# -*- coding: utf-8 -*-

import grequests
from bs4 import BeautifulSoup, Comment
import re


def patch_text(text):
    return re.sub(r'(\A|\W)(\w{6})(\W|\Z)', u'\\1\\2™\\3', text, flags=re.U)

example_urls = [
    ('http://habrahabr.ru/company/yandex/blog/258673/', 'Original habr post'),
    ('http://www.sony.com', 'Sony Official Website'),
    ('http://dreampuf.github.io/GraphvizOnline/', 'GraphViz Online'),
    ('http://httpbin.org/forms/post', 'HTML form for POST test')
    ]


def url_host_len(url):
    if len(url) == 0:
        return -1
    if url[0] == '/':
        return 0
    # TODO: verify scheme (http[s])
    pos = url.find('://')
    if pos == -1:
        return 0
    pos = url.find('/', pos+3)
    if pos == -1:
        return len(url)
    else:
        return pos


# TODO: test non-latin urls
def process_html(proxy_host, host, path, html, transform_text):
    url_tags = [(None, 'href'), (None, 'src'), ('form', 'action')]
    skip_tags = ['script', 'style', '[document]']

    def fix_url(url):
        print 'fix ', url
        # FIXME: length check
        if url[:2] == '//':
            proto = host[:host.find(':')+1]
            url = proto + url
        elif url_host_len(url) == 0:
            if url[0] == '/':
                url = host + url
            else:
                print host, path, url
                print type(host), type(path), type(url)
                url = host + path + url
        return proxy_host + url

    soup = BeautifulSoup(html, "html.parser")

    for name, attr in url_tags:
        print 'patch ', attr
        # ORIGIN: http://stackoverflow.com/a/6758951
        for tag in soup.find_all(name, **{attr: True}):
            tag[attr] = fix_url(tag[attr].encode('utf-8')).decode('utf-8')

    for text in soup.find_all(text=lambda text: not isinstance(text, Comment) and text.parent.name not in skip_tags):
        patch = transform_text(text)
        if patch is not None:
            print 'type: ', type(text), 'parent type: ', type(text.parent), 'name: ', text.parent.name, ' patch text: ', text
            text.replace_with(patch)
    return unicode(soup)

def request(url, query, method, headers, body):
    print('remote request url: %s headers: %s' % (url, headers))
    res = grequests.map([grequests.request(method, url, params=query, data=body, headers=headers, stream=True, allow_redirects=False, verify=False)])[0]
    if res is None:
        return (None,)*5
    def drop_header(name):
        try:
            del res.headers[name]
        except KeyError:
            pass
    print('remote response headers: ', res.headers)
    # needs to remove the compression 'cause the raw.read() (http://stackoverflow.com/a/18364322) isn't working in grequests
    drop_header('transfer-encoding')
    drop_header('content-encoding')
    drop_header('content-length')
    if not res.headers.get('content-type', '').startswith('text/html'):
        print('returning data as is')
        return res.status_code, res.reason, res.headers, res.iter_content(chunk_size=None), None

    drop_header('content-type')
    print 'encoding:', res.encoding
    if res.encoding == 'ISO-8859-1': # HACK: rather unicode by default
        res.encoding = 'utf-8'
    return res.status_code, res.reason, res.headers, None, res.text


def proxy_call(proxy_host, url, query, method, headers, body_file):
    def response_error():
        return '502 Bad Gateway', {}, ['Maybe URL "%s" is wrong..' % url]

    def response_redirect(url):
        location = {'Location': proxy_host + url}
        if method == 'POST':
            # ORIGIN: http://programmers.stackexchange.com/a/99966
            return '307 Temporary Redirect', location, ['']
        return '301 Moved Permanently', location, ['']

    def response_notfound():
        return '404 Not Found', {}, [
            '<html><body>Wrong URL format. Examples:<br/>' +
            '<br/>'.join(map(lambda site: ' <a href="' + proxy_host + site[0] + '">' + site[1] + '</a>', example_urls)) +
            '</body></html>']

    # TODO: patch Origin:
    # patch Referer:
    try:
        referer = headers['Referer']
        if referer.startswith(proxy_host):
            referer = referer[len(proxy_host):]
            headers['Referer'] = referer
        else:
            print('!!Strange referer: ', referer)
    except KeyError:
        referer = None
    print referer, url
    host_len = url_host_len(url)
    # redirect
    if host_len == 0:
        if referer is not None and referer != '':
            host_len = url_host_len(referer)
            host = referer[:host_len]
            url = host + '/' + url
        else:
            url = 'http://' + url
        return response_redirect(url)
    elif host_len > 0:
        host = url[:host_len]
    else:
        print headers
        return response_notfound()

    print 'host=', host

    code, reason, headers, data_iter, html = request(url, query, method, headers, body_file)
    if code == None:
        return response_error()
    elif code == 200 and html is not None:
        html = process_html(proxy_host, url[:host_len], url[host_len:], html, patch_text)
    elif 300 <= code < 400:
        try:
            headers['Location'] = proxy_host + headers['Location']
        except KeyError:
            pass

    if html is not None:
        headers['Content-Type'] = 'text/html; charset=utf-8'
        body_file = [html.encode('utf-8')]
    else:
#        print 'len=', len(data_iter)
        body_file = data_iter
    return str(code) +' ' + reason, headers, body_file


def application(environ, start_response):
# ORIGIN: https://www.python.org/dev/peps/pep-3333/#url-reconstruction
    host = environ['wsgi.url_scheme'] + '://'

    if environ.get('HTTP_HOST'):
        host += environ['HTTP_HOST']
    else:
        host += environ['SERVER_NAME']

        if environ['wsgi.url_scheme'] == 'https':
            if environ['SERVER_PORT'] != '443':
                host += ':' + environ['SERVER_PORT']
        else:
            if environ['SERVER_PORT'] != '80':
                host += ':' + environ['SERVER_PORT']
    host += '/'

    headers = {'-'.join(map(str.capitalize, var[5:].split('_'))): val
               for var, val in environ.items() if var.startswith('HTTP_')}

    del headers['Host']

    code, headers, body = proxy_call(host, environ['PATH_INFO'][1:], environ.get('QUERY_STRING'),
                                     environ['REQUEST_METHOD'].upper(), headers, environ['wsgi.input'])

    start_response(code, [(k, v) for k, v in headers.items()])
    return body


if __name__ == '__main__':
    from gevent.pywsgi import WSGIServer
    print('Serving on 8088...')
    WSGIServer(('', 8088), application).serve_forever()
