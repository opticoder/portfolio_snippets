## Simple tree structure ##
A tree structure with full text search with 2 intefaces (RESTful and Telegram) and 2 storage backends (MongoDB, PostgreSQL). It was made as solution for these two tasks:

### Task 1: Tree structure with RESTful interface ###

Реализовать бекенд работающий на Asyncio и MongoDB/PostgreSQL (на выбор) с REST-интерфейсом.
Бекенд должен предоставлять возможность работы с древовидной структурой которую надо правильно уложить в базу.
Каждый элемент дерева должен иметь обязательное поле text, по которому осуществляется полнотекстовый поиск.

Основные требования:

* Авторизация пользователя по уже существующему логину и паролю, дальнейшее взаимодействие с api возможно только при авторизации
* Метод для вставки нового элемента
* Метод для полнотекстового поиска с выдачей пути до корневого элемента
* Метод для извлечения поддерева по id элемента
* В качестве gui для методов должен быть использован swagger-ui

Ответ на все методы должен быть json.

### Task 2: Telegram bot ###

Реализовать телеграм бота с бесконечным циклом посылки случайных вопросов из фиксировано заданного массива с предопределенными ответами.

Требования:

* Бот должен быть асинхронным (использовать asyncio, сетевой ввод/вывод, база данных)
* Бот должен работать с базой данных PostgreSQL (используя aiopg и sqlalchemy)
* Бот должен получать уведомления об обновлениях от сервера телеграм через проксирующий сервер (webhook, nginx)
* Бот должен обрабатывать ошибки внешних сервисов (телеграм, база данных)
* Желательно, чтобы бот собирал статистику по взаимодействию с пользователем (старт сессии, продолжительность сессии)

## Choices (common part) ##
- integrated full text search to MongoDB: https://docs.mongodb.com/manual/reference/operator/query/text/
- ORM MongoMotor for MongoDB (MongoEngine port for asyncio, I had an experience with this ORM and was curious to have a look to its readiness for asyncio)
- integrated full text search to PostgreSQL: https://www.postgresql.org/docs/current/static/textsearch.html
- DB model of tree structure: Array of Ancestors. Also it was made a stub (db_mock.py) for debuging without a real DB

## Notes ##
- it happened that it's possible to store several trees in the DB, any grouping wasn't implemented, every search works with all stored trees
- no convenient logging, just a few debug prints
