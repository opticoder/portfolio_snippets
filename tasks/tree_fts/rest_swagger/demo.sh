#!/bin/sh

PORT=${PORT:=8081}

json_field()
{
python -c "import sys, json; print(json.load(sys.stdin)['$1'])"
}

json=`curl -X GET --header "Authorization: Basic dXNyOnBheg==" "http://localhost:$PORT/v15/auth" 2>/dev/null`
token=`echo $json | json_field token`
echo auth token: $token

json=`curl -X POST -d '{"id": null, "text": "text*"}' "http://localhost:$PORT/v15/add?token=$token" 2>/dev/null`
root_id=`echo $json | json_field id`
echo root id: $root_id

for n in 0 1;
do
  json=`curl -X POST -d "{\"id\": \"$root_id\", \"text\": \"text0\"}" "http://localhost:$PORT/v15/add?token=$token" 2>/dev/null`
done
node_id=`echo $json | json_field id`
echo node id: $node_id

for n in 0 1;
do
  curl -X POST -d "{\"id\": \"$node_id\", \"text\": \"text1\"}" "http://localhost:$PORT/v15/add?token=$token"
done

echo full tree:
curl -X POST -d "{\"id\": \"$root_id\"}" "http://localhost:$PORT/v15/fetch?token=$token" 2>/dev/null | python -mjson.tool
echo subtree:
curl -X POST -d "{\"id\": \"$node_id\"}" "http://localhost:$PORT/v15/fetch?token=$token" 2>/dev/null | python -mjson.tool
echo search:
curl -X POST -d "{\"text\": \"text1\"}" "http://localhost:$PORT/v15/search?token=$token" 2>/dev/null | python -mjson.tool
