# -*- coding: utf-8 -*-

import config
from aiohttp import web
from auth import auth_middleware, auth_handler
from api import add_handler, search_handler, subtree_handler

def jsonify_middleware(handler, no_input=False):
    async def req(request):
        return web.json_response(await handler(request if no_input else await request.json()))
    return req

routes = [
    ('GET', '/v15/auth', jsonify_middleware(auth_handler, True)),
    ('POST', '/v15/add', auth_middleware(jsonify_middleware(add_handler))),
    ('POST', '/v15/search', auth_middleware(jsonify_middleware(search_handler))),
    ('POST', '/v15/fetch',  auth_middleware(jsonify_middleware(subtree_handler))),
]
app = web.Application()
for route in routes:
    app.router.add_route(*route)

#app.router.add_static('/swagger', "/var/www/swagger-ui")
app.router.add_static('/swagger', "sw-ui")
app.router.add_static('/spec', "spec")

if __name__ == '__main__':
    import asyncio
    loop = asyncio.get_event_loop()
    loop.run_until_complete(config.db.init_connect(config.db_conn))
    web.run_app(app, port=8081)
