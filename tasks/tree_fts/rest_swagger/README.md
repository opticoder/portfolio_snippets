### Choices ###
- стандартная HTTP Basic Authorization для получения токена сессии
- aiohttp для обработки HTTP запросов и запуска тестового приложения

### Simplifications ###
- в авторизации нет работы с базой/сессиями, В коде логин/пароль и ключ сессии захардкожены (auth.py)
- для авторизации так же как и для остальных запросов используется только HTTP
- нет тестов, только демонстрационный скрипт (demo.sh)
- нет обработки ошибок
- there's no convenient config, most of parameters are hardcoded

### Run ###
cd rest_swagger
pip install -r requirements.txt
./install_ui.sh # загрузка и распаковка сборки Swagger UI
DB_CONN='mongodb://localhost/tree-task' python3.5 app.py # or DB_CONN='postgresql://bot:passwd@127.0.0.1/botdb' or empty for in memory model
Интерфейс Swagger'а http://127.0.0.1:8080/swagger/index.html
Путь к спецификации RESTful API http://127.0.0.1:8080/spec/swagger.yaml
Скрипт demo.sh: авторизуется, создаёт дерево с 2 вершинами и у одной из них ещё 2 подвершины, получает поддерево всего дерева, части дерева, поиск вершин содержащих "text1"

### Notes ###
- сам Swagger UI заставить делать запросы не удалось, через curl всё работает
- в реальности использовался Vagrant и Ansible для раскатки базы, Скрипты не выложены, их оформление требует некоторого времени

### Example output (demo.sh) ###
auth token: Val1Dt0Ken
root id: 57c72ac10640fd224c2dcb0c
node id: 57c72ac10640fd224c2dcb0e
full tree:
{
    "text": "text*",
    "subs": [
        {
            "text": "text0",
            "subs": [],
            "id": "57c72ac10640fd224c2dcb0d"
        },
        {
            "text": "text0",
            "subs": [
                {
                    "text": "text1",
                    "subs": [],
                    "id": "57c72ac10640fd224c2dcb0f"
                },
                {
                    "text": "text1",
                    "subs": [],
                    "id": "57c72ac10640fd224c2dcb10"
                }
            ],
            "id": "57c72ac10640fd224c2dcb0e"
        }
    ],
    "id": "57c72ac10640fd224c2dcb0c"
}
subtree:
{
    "text": "text0",
    "subs": [
        {
            "text": "text1",
            "subs": [],
            "id": "57c72ac10640fd224c2dcb0f"
        },
        {
            "text": "text1",
            "subs": [],
            "id": "57c72ac10640fd224c2dcb10"
        }
    ],
    "id": "57c72ac10640fd224c2dcb0e"
}
search:
[
    {
        "text": "text1",
        "id": "57c72ac10640fd224c2dcb0c/57c72ac10640fd224c2dcb0e/57c72ac10640fd224c2dcb0f"
    },
    {
        "text": "text1",
        "id": "57c72ac10640fd224c2dcb0c/57c72ac10640fd224c2dcb0e/57c72ac10640fd224c2dcb10"
    }
]
