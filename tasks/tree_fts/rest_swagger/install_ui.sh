#!/bin/sh

ver="${2:-2.2.2}"
inst_dir="$1"
[ -z "$inst_dir"] && inst_dir="./sw-ui"
mkdir -p "$inst_dir"
tmp_arch="$inst_dir/tmp.tgz"
inst_base=`dirname "$inst_dir"`

wget -c https://github.com/swagger-api/swagger-ui/archive/v$ver.tar.gz -O "$tmp_arch"
tar -xzf "$tmp_arch" -C "$inst_dir"
mv "$inst_dir" "$inst_base/tmp"
mv "$inst_base/tmp/swagger-ui-$ver/dist" "$inst_dir"
rm -rf "$inst_base/tmp"
