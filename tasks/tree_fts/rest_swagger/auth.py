# -*- coding: utf-8 -*-

import aiohttp

async def auth_handler(request):
    auth_header = request.headers.get('AUTHORIZATION')
    if auth_header is None:
        raise aiohttp.web.HTTPForbidden()
    auth = aiohttp.BasicAuth.decode(auth_header)
#    print(auth)
    if auth.login == 'usr' and auth.password == 'paz':
        return {'token' : 'Val1Dt0Ken'}
    else:
        raise aiohttp.web.HTTPForbidden()

def auth_middleware(handler):
    async def middleware(request):
        if request.query_string == 'token=Val1Dt0Ken':
            return await handler(request)
        else:
            raise aiohttp.web.HTTPUnauthorized()
    return middleware
