# -*- coding: utf-8 -*-

from config import db

async def _node_view(id, text):
    return {'id': id, 'text': text, 'subs': await db.search_subs(id)}

async def get_subtree(parent):
    tree = await _node_view(parent, await db.read_node(parent))
    q = [tree]
    while (len(q)):
        pnode = q.pop()
        plist = pnode['subs']

        pnode['subs'] = []
        for id, text in plist:
            node = await _node_view(id, text)
            pnode['subs'].append(node)
            q.append(node)
    return tree


async def add_handler(params):
    print(params)
    return {'id': await db.add_node(params['id'], params['text'])}

async def search_handler(params):
    print(params)
    return [{'id': '/'.join(node[0]), 'text': node[1]} for node in await db.search_nodes(params['text'])]

async def subtree_handler(params):
    print(params)
    return await get_subtree(params['id'])
