# -*- coding: utf-8 -*-

from mongomotor.fields import StringField, ListField, ReferenceField
from mongomotor import Document, connect
from bson.objectid import ObjectId

class Node(Document):
    text = StringField()
    ancestors = ListField(ReferenceField('self'))
    parent = ReferenceField('self')

    # ORIGIN: https://github.com/MongoEngine/mongoengine/pull/700
    meta = {'indexes': [{'fields': ['$text'], 'default_language': 'english'}, 'parent']}

async def add_node(parent, text):
    if parent is not None:
        ancestors = await Node.objects(id=ObjectId(parent)).no_dereference().first()
        ancestors = ancestors.ancestors + [ObjectId(parent)]
    else:
        ancestors = []
    node = Node(text=text, ancestors=ancestors, parent=parent)
    await node.save()
    return str(node.id)

async def search_nodes(text):
    # ORIGIN: https://github.com/MongoEngine/mongoengine/pull/700#commitcomment-18597086
    result = []
    async for node in Node.objects.no_dereference().search_text(text).order_by('$text_score'):
        result.append((list(map(lambda x: str(x.id), node.ancestors)) + [str(node.id)], node.text))
    return result

async def read_node(id):
    node = await Node.objects(id=ObjectId(id)).first()
    return node.text

async def search_subs(parent):
    result = []
    async for node in Node.objects(parent=parent):
        result.append((str(node.id), node.text))
    return result

async def init_connect(conn_string):
    connect('tree-task', host="localhost", port=27017, async_framework='asyncio')
#    connect(host=conn_string, async_framework='asyncio')
    Node.ensure_indexes()
