# -*- coding: utf-8 -*-

# PREFACE: https://github.com/aio-libs/aiopg/issues/19

from sqlalchemy import Table, Column, Index, ForeignKey, MetaData
from sqlalchemy.types import ARRAY, Integer, UnicodeText
from sqlalchemy.dialects.postgresql import TSVECTOR
from sqlalchemy import event, DDL
from sqlalchemy import select
from sqlalchemy.sql.expression import func

metadata = MetaData()

nodes = Table('nodes', metadata,
                Column('id', Integer, primary_key=True),
                Column('text', UnicodeText),
                Column('ancestors', ARRAY(Integer)),
                Column('parent', ForeignKey('nodes.id'), index=True),
                Column('tsv_text', TSVECTOR),
                Index('idx_tsv_text', 'tsv_text', postgresql_using = 'gin'),
              )
trig_nodes_tsv_text = DDL("""
    CREATE TRIGGER tsv_text_update BEFORE INSERT OR UPDATE
    ON nodes
    FOR EACH ROW EXECUTE PROCEDURE
    tsvector_update_trigger(tsv_text, 'pg_catalog.english', text);
""")
event.listen(nodes, 'after_create', trig_nodes_tsv_text.execute_if(dialect='postgresql'))

async def add_node(parent, text):
    async with engine.acquire() as conn:
        if parent is not None:
            ancestors = await conn.scalar(select([nodes.c.ancestors]).where(nodes.c.id == parent))
            ancestors.append(int(parent))
        else:
            ancestors = []
        id = await conn.scalar(nodes.insert().values(text=text, ancestors=ancestors, parent=parent))
        return str(id)

async def search_nodes(text):
    result = []
    async with engine.acquire() as conn:
        rank = func.ts_rank(nodes.c.tsv_text, func.to_tsquery(text)).label('rank')
        async for node in conn.execute(select([nodes.c.text, nodes.c.ancestors, nodes.c.id, rank])
                                               .where(nodes.c.tsv_text.match(text)).order_by(rank.desc())):
            print(node)
            result.append((list(map(lambda x: str(x), node.ancestors)) + [str(node.id)], node.text))
    return result

async def read_node(id):
    async with engine.acquire() as conn:
        text = await conn.scalar(select([nodes.c.text]).where(nodes.c.id==int(id)))
    return text

async def search_subs(parent):
    result = []
    async with engine.acquire() as conn:
        async for node in conn.execute(select([nodes.c.id, nodes.c.text]).where(nodes.c.parent==parent)):
            result.append((str(node.id), node.text))
    return result

async def init_connect(conn_string):
    async def ensure_scheme():
        # HACK: https://github.com/aio-libs/aiopg/issues/123
        import sqlalchemy as sa
        sync_engine = sa.create_engine(conn_string)
        import asyncio
        loop = asyncio.get_event_loop()
        await loop.run_in_executor(None, metadata.create_all, sync_engine)

    global engine
    from aiopg.sa import create_engine
    import logging
    logging.basicConfig()
    logging.getLogger('sqlalchemy').setLevel(logging.DEBUG)
    engine = await create_engine(conn_string)

#    await metadata.create_all(engine)
    await ensure_scheme()
