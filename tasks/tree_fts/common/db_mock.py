# -*- coding: utf-8 -*-

async def add_node(parent, text):
    global db, idn
    ids = str(idn)
    idn += 1
    if parent is not None:
        ancestors = db[parent]['ancestors'] + [parent]
    else:
        ancestors = []
    db[ids] = {'text': text, 'ancestors': ancestors, 'parent': parent}
    print(db)
    return ids

async def search_nodes(text):
    return [(node['ancestors'] + [id], node['text']) for id, node in db.items() if text in node['text']]

async def read_node(id):
    return db[id]['text']

async def search_subs(parent):
    return [(id, node['text']) for id, node in db.items() if parent == node['parent']]

async def init_connect(conn_string):
    global db, idn
    db = {}
    idn = 0
