import os
import sys

sys.path.append(os.path.abspath(os.path.join('..', 'common')))
db_conn = os.environ.get('DB_CONN', '')
if db_conn.startswith('postgresql:'):
    import db_pg as db
elif db_conn.startswith('mongodb:'):
    import db_mongo as db
else:
    import db_mock as db

bot_token = os.environ['API_TOKEN']
