# -*- coding: utf-8 -*-

import config
import aiohttp
from api import add_handler, search_handler, subtree_handler
from exbot import ExBot
import config

#bot = ExBot(api_token=config.bot_token, path='/eeeeee', webhook=('https://emyxample.com/secreturl', None))
bot = ExBot(api_token=config.bot_token)

@bot.command(r"/add (\S+) (.+)")
async def command(chat, match):
    id = match.group(1)
    if id == '*':
        id = None
    info = await add_handler({'id': id, 'text': match.group(2)})
    await chat.send_text(info['id'])

@bot.command(r"/find (.+)")
async def command(chat, match):
    info = await search_handler({'text': match.group(1)})
    if len(info) > 0:
        await chat.send_text('\n'.join([node['id']+' ' + node['text'] for node in info]))
    else:
        await chat.send_text('not found')

def graph2dot(vertexes, edges):
    gvdata = 'digraph G {'
    for i, v in enumerate(vertexes):
        gvdata += 'subgraph cluster%d { label = "%s"; color=white; node [style=filled]; %s; }' % (i, v[1], v[0])
    for node in edges:
        gvdata += '{} -> {};'.format(node[0], node[1])
    gvdata += '}'
    return gvdata


def tree2graph(tree):
    vertexes = []
    edges = []
    q = [tree]
    while (len(q)):
        pnode = q.pop()
        vertexes.append((pnode['id'], pnode['text']))
        for node in pnode['subs']:
            edges.append((pnode['id'], node['id']))
            q.append(node)
    return vertexes, edges

from io import BytesIO

# ORIGIN: http://sandbox.kidstrythisathome.com/erdos/
async def draw_graphviz(dot_graph):
    # FIXME: new get
    async with aiohttp.get('http://chart.googleapis.com/chart', params={'chl': dot_graph, 'cht': 'gv'}) as s:
        return BytesIO(await s.read())


@bot.command(r"/show (\S+)")
async def command(chat, match):
    id = match.group(1)
    tree = await subtree_handler({'id': id})
    dot_data = graph2dot(*tree2graph(tree))
    f = await draw_graphviz(dot_data)
    f.name = 'ept.png' # ORIGIN: https://github.com/eternnoir/pyTelegramBotAPI/issues/91#issuecomment-152862610
    await chat.send_photo(photo=f)

if __name__ == '__main__':
    import asyncio
    loop = asyncio.get_event_loop()
    loop.run_until_complete(config.db.init_connect(config.db_conn))
#    loop.run_until_complete(bot.set_webhook('', None))
    bot.run()
#    bot.run_webhook(port=8081)
