### Choices ###
- aiotg library for communication with Telegram bot API
- extension of aiotg for using with webhook method (exbot.py)
- picture of the representation of tree state by using Graphviz online (Google Charts)
- aiohttp for webhook mode

### Simplifications ###
- webhook mode was only partly tested and documented, not really used
- possibility of aiotg for analytics isn't used

### Run ###
cd telegram_bot
pip install -r requirements.txt
API_TOKEN='yourbot:token' DB_CONN='mongodb://localhost/tree-task' python3.5 bot.py # or DB_CONN='postgresql://bot:passwd@127.0.0.1/botdb' or empty for in memory model
/add * text1, /add id text2, /find text, /show subtree_id

### Example output ###
![/find command](screenshot_find.jpg)
![/show command](screenshot_show.jpg)
