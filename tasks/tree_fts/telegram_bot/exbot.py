# -*- coding: utf-8 -*-

"""Extension for webhook mode for Telegram bot framework :class:`aiotg`
"""

from aiohttp import web
from aiotg import Bot

class ExBot(Bot):
    """Extension for webhook mode for :class:`aiotg.bot.Bot`

    Parameters are the same as for :func:`aiotg.bot.Bot`
    plus two:

    :param tuple webhook (optional): Turn on webhook mode. A tuple (`url`, `cert`) for webhook url and certificate.
    See: https://core.telegram.org/bots/api#setwebhook for more information
    :param str path (optional): Enables validation of path on every http request
    """

    def __init__(self, webhook=None, path=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._webhook = webhook
        self._path = path
        self._app = None

    async def _handler(self, request):
        self._process_updates({'ok': True, 'result': [await request.json()]})
        return web.json_response({})

    def set_webhook(self, url=None, certificate=None):
        """
        The separate method to set webhook for Telegram bot
        Returns True on success.

        :param str url: URL of the webhook for the bot
        :param str certificate: Certificate file for the bot
        See: https://core.telegram.org/bots/api#setwebhook for more information
        """
        if url == None:
            url = ''
        json_result = self.api_call('setWebhook', url=url, certificate=certificate)
        return json_result['result']

    def run_webhook(self, *args, **kwargs):
        """
        Convenience method for running bot in webhook mode

        :Example:

        >>> if __name__ == '__main__':
        >>>     bot.run_webhook(port=8081)
        """
        if not self._app:
            self._app = web.Application()
            self._app.router.add_route('POST', self._path if self._path else '/{path}', self._handler)

        if self._webhook is not None:
            self._app.loop.run_until_complete(self.set_webhook(*self._webhook))
        web.run_app(self._app, *args, **kwargs)
