# README #

This is some code from the last projects i participated in. i worked merely in commercial projects so i can't open the full sources, but i cat pull out some parts of them.
Directory "snippets" contains the parts of code of some projects.
Also directory "tasks" contains the code of some test tasks i performed while i was looking for job.

### game_server1 ###

Parts of code for "Adventure Era" game server
http://www.apppicker.com/reviews/13861/adventure-era-app-review-yet-another-building-and-collecting-game

Built with:

* Python 2, gevent, Flask (admin UI & testing)
* PostgreSQL, SQLAlchemy
* C++ to Python wrapping (game logic), CMake for building C++ code
* nginx, uwsgi, Ansible (for deploying and configuration)
* virtualization with Vagrant for developing and debugging
* Own test framework for functional testing (on bash & D language)

### proxy ###

Some kind of HTTP proxy that changes text of HTML pages

### tree_fts ###

A simple tree structure with full text search with 2 intefaces (RESTful and Telegram) and 2 storage backends (MongoDB, PostgreSQL).
